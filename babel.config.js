module.exports = {
  presets: [
    "@babel/preset-env",
    "@babel/preset-typescript", // Ambiente que a aplicação está sendo executada
    [
      "@babel/preset-react",
      {
        runtime: "automatic",
      },
    ],
  ],
};
